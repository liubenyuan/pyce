# -*- coding: utf-8 -*-
"""
main function of kaggle.connectomics

"""
import numpy as np
import data_io
import scoring as sc

def main():
    """
    select the set name in SETTINGS.json
    """
    set = 'small_04';
    set_network = set + '_network';
    set_fluore = set + '_fluorescence';

    """
    load fluorescence and network,
    and build up graph<NxN> connectivity matrix
    """
    data_fc = data_io.load_data(set_fluore);
    data_net = data_io.load_data(set_network,dtype=np.int);
    graph = sc.trueScore(data_net,data_fc.shape[1]);
    print graph.shape
    
    """
    process ground truth
    """
    graph_unibi = ((graph>0) | (graph.T>0));
    print 'ROC_AUC =', sc.roc_auc(graph,graph_unibi), 'PR_AUC =', sc.pr_auc(graph,graph_unibi)
    
    """
    calculate AUC with random guess
    """
    scoreRandom = sc.randomScore(data_fc);
    print 'ROC_AUC =', sc.roc_auc(graph,scoreRandom), 'PR_AUC =', sc.pr_auc(graph,scoreRandom)
    
    """
    discretize/binning the fluorescence signals
    """
    data_dfc,G = sc.discretize_fluorescence(data_fc,bins=[0.122],clevel=0.25,debug=True);

    """
    calculate AUC with pearson/xor correlation with discretization
    """
    scoreCorr = sc.corr_spikes(data_dfc)
    print 'ROC_AUC =', sc.roc_auc(graph,scoreCorr), 'PR_AUC =', sc.pr_auc(graph,scoreCorr)
    print scoreCorr.max(), scoreCorr.min()
#    scoreCorr = sc.xor_spikes(data_dfc)
#    print 'ROC_AUC =', sc.roc_auc(graph,scoreCorr), 'PR_AUC =', sc.pr_auc(graph,scoreCorr)
    
    """
    write model-free predictions as kaggle format
    """
#    set_valid = "valid_fluorescence";
##    set_test = "test_fluorescence"
#    valid_fc = data_io.load_data(set_valid);
##    test_fc = data_io.load_data(set_test);
#    valid_dfc,_ = sc.discretize_fluorescence(valid_fc,bins=[0.122],clevel=0.25,debug=True);
#    scoreRandomValid = sc.corr_spikes(valid_dfc);
#    print scoreRandomValid.min(), scoreRandomValid.max()
#    data_io.write_submission(scoreRandomValid,scoreRandomValid);

# note : execute the main code by default
if __name__=="__main__":
    main();