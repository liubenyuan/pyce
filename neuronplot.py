"""
Visualize neuron activity
"""
import matplotlib.pyplot as plt
import data_io
import scoring

set = 'small_04'
set_fluore = set + '_fluorescence';
set_network = set + '_network';
data_fc = data_io.load_data(set_fluore);
data_net = data_io.load_data(set_network,dtype=int)
N = data_fc.shape[1]; # number of neurons
graph = scoring.trueScore(data_net,N); # ground-truth connect graph

# preprocess 1. discretized and binning
data_spike,_ = scoring.discretize_fluorescence(data_fc,bins=[0.122],clevel=0.25,debug=True)
# preprocess 2. oopsi

# step 1 : generate figure
fig = plt.figure()

N = 2000;
I = 87; # index start from [0]
J = 5;
title = 'Top: ' + str(N) + ' Activity series for neuron ' + str(I+1) + ' and ' + str(J+1) + \
        ' superimposed.\n Bottom: Discretized spikes superimposed. Truth I->J is ' + \
        str(graph[I,J]) + ' and J->I is ' + str(graph[J,I]);

# step 2 : build title
fig.suptitle(title)
# step 3 : add axis
ax1 = fig.add_subplot(211)
ax2 = fig.add_subplot(212)

# step 4 : plot graph in ax1 and ax2
ax1.plot([f for f in xrange(2000)],data_fc[:2000,I],color="blue")
ax1.plot([f for f in xrange(2000)],data_fc[:2000,J],color="green")
#
ax2.plot([f for f in xrange(2000)],data_spike[:2000,I],color="blue", linewidth=1.5)
ax2.plot([f for f in xrange(2000)],data_spike[:2000,J],color="green", linewidth=1.5)
# plt.savefig("neuronplot.png")
plt.show()

