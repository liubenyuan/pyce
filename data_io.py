"""
file I/O utilities
Author : liubenyuan AT gmail DOT com
----
learn python from jarfo@github,
"""

import json
import os
import os.path
import pandas as pd
import numpy as np

# note: json.loads operate on strings.
def get_path(key):
    paths = json.loads(open('SETTINGS.json').read());
    return os.path.expandvars(paths[key])

# inline function, just like f = @(x) in matlab
def parse_dataframe(df):
    parse_cell = lambda cell: np.fromstring(cell, dtype=np.float, sep=" ")
    df = df.applymap(parse_cell)
    return df

# simply copy from jarfo's code
def read_pairs(set):
    path = get_path(set)
    return parse_dataframe(pd.read_csv(path,index_col='SampleID')) 
    
# alternatively, going light-weight, you may consider joe kington's code
# http://stackoverflow.com/questions/8956832/python-out-of-memory-on-large-csv-file-numpy
def iter_loadtxt(filename, delimiter=',', skiprows=0, dtype=np.float):
    def iter_func():
        with open(filename, 'r') as infile:
            for _ in range(skiprows):
                next(infile)
            for line in infile:
                line = line.rstrip().split(delimiter)
                for item in line:
                    yield dtype(item)
        iter_loadtxt.rowlength = len(line)

    data = np.fromiter(iter_func(), dtype=dtype)
    data = data.reshape((-1, iter_loadtxt.rowlength))
    return data
    
# so, can we read data more efficiently ?
# numpy: np.genfromtxt or np.loadtxt, use loadtxt when no value is missing
#   np.genfromtxt(path, delimiter=',', dtype=np.float)
# also, you may use pandas for speed, watch the 
#   pd.read_csv(path,header=None,delimiter=',',dtype=np.float).values
# joe kington's code, see implementation.
#   iter_loadtxt(path)
def load_data(set,dtype=np.float):
    path = get_path(set)
    return pd.read_csv(path,header=None,delimiter=',',dtype=dtype).values
    
# how to save to a csv file
def generate_text_file(length=1e6, ncols=20):
    data = np.random.random((length, ncols))
    np.savetxt('large_text_file.csv', data, delimiter=',')

# write data to a csv file. I->J,S
# using with block, it automatically close file handler
def write_submission(valid,test):
    path = get_path('kaggle_submission');
    N1 = valid.shape[1]
    N2 = test.shape[1]
    with open(path, "wb") as outfile:
        outfile.write("NET_neuronI_neuronJ,Strength\n")
        for i in xrange(N1):
            for j in xrange(N1):
                outfile.write("valid" + "_" + str(i+1) + "_" + str(j+1) + "," + str(valid[i,j]) + "\n")
        print 'valid predictions are written.'
        for i in xrange(N2):
            for j in xrange(N2):
                outfile.write("test" + "_" + str(i+1) + "_" + str(j+1) + "," + str(test[i,j]) + "\n")
        print 'test predictions are written.'
                
