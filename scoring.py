# -*- coding: utf-8 -*-
"""
scoring methods
    * trueScore : generate ground truth matrix
    * randomScore : generate random metrics
    * computeXC : calculate pearson correlation with discretization
    * computeGTE : python analog of olav stetter's code
liubenyuan <liubenyuan AT gmail DOT com>
----
learn python from kaggle.connectomics
"""

import numpy as np
import scipy.sparse as sparse
import scipy.stats as stats
from sklearn.metrics import roc_auc_score
from sklearn.metrics import auc
from sklearn.metrics import precision_recall_curve

# calculate true Connectivity matrix
# input : v(I,J,S) is Px3 sparse network matrix
#         N        is the size of Connectivity matrix
def trueScore(v,N):
    graph = np.zeros((N,N),dtype=np.float);
    graph[v[:,0]-1,v[:,1]-1] = v[:,2];
    graph[graph == -1] = 0;
    return graph

# graph_unibi = ((graph>0) | (graph.T>0));
# ROC_AUC : AUC of tp and fp
def roc_auc(graph,pred):
    return roc_auc_score(np.ravel(graph),np.ravel(pred))
    
# PR_AUC : AUC of precision and recall
def pr_auc(graph,pred):
    [p,r,_] = precision_recall_curve(np.ravel(graph),np.ravel(pred))
    return auc(r,p)
    
# generate random scores, using sparse matrix, where v.col, v.row, v.data are used
# you may use toarray or tomatrix to convert sparse -> dense
# ret(I,J) = Strength(I->J)
def randomScore(F,debug=False):
    N = F.shape[1]
    if debug: print 'random score calculated!'
    return sparse.rand(N,N,density=0.1).toarray()

# apply diff/binnig on Time-Samples (axis-0)
def discretize_fluorescence(F, clevel=0, bins=[0.12], globalBins=True, debug=False):
    """
    < inputs >:
    F          - The fluorescence signals, each row is samples and each column 
                 is the fluorescence of a neuron. F is a np.array object
    clevel     - conditional level, if (0): automatic leveling
    bins       - if (bins is not scalar): use it as default binEdges,
                 else: use automatic binning, where
    globalBins - if (True): use global min-max in matrix,
                 else: auto-bins is applied per neuron (column) 
    debug      - print some message
    < outputs >:
    D - The discretized signal (each row a sample, aech column a neuron).
    G - Vector defining the global conditioning level of the signal at
        that given time (for now 1 and 2 for below and above the level).
    """
    # step 1. alculate condition values and G
    avf = F.mean(axis=1)
    if clevel==0:
        hits,pos = np.histogram(avf,bins=100)
        clevel = pos[np.argmax(hits)] + 0.05
    if debug: print 'conditional level=',clevel
    G = (avf >= clevel) + 1
    G = G[1:]
    # step 2. high-pass filtered and D(discretize)
    v = np.diff(F,axis=0) 
    epsilon = 1e-3
    vmax = v.max() + epsilon
    vmin = v.min() + epsilon
    D = np.zeros(v.shape); # pre-allocate the storage (do we need?)
    for i in xrange(v.shape[1]): # np.apply_along_axis(lambda x : np.digitize(x,bins),0,v);
        if np.isscalar(bins)==False:
            binEdges = np.array(bins)
        elif globalBins:
            binEdges = np.linspace(vmin, vmax, bins+1)
        else:
            vi = v[:,i]
            binEdges = np.linspace(vi.min()-epsilon, vi.max()+epsilon, bins+1)
        D[:,i] = np.digitize(v[:,i], binEdges);
    if debug: print 'discretization done!'
    return D, G

# pearson, (using abs)?
# stats.pearsonr(D[:,i], D[:,j])[0], or np.corrcoef(D[:,i],D[:,j])[0,1]
def corr_spikes(D):
    M = np.corrcoef(D.T)
    np.fill_diagonal(M,0)
    return M
    
# xor, simply corr np.correlate(D[:,i], D[:,j]) results bad scores
def xor_spikes(D):
    L,N = D.shape
    ret = np.zeros((N,N))
    for i in xrange(N):
        for j in xrange(i+1,N):
            val = np.logical_not(np.logical_xor(D[:,i],D[:,j])).sum() / L
            ret[i,j] = val
            ret[j,i] = val
    return ret

# GTE

# MI
